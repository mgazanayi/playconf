package models;

import play.data.validation.Constraints.*;
import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Speaker extends Model {

    @Id
    public Long id;

    @Required
    public String name;

    @Required
    public String email;

    @Required
    @MinLength(10)
    @MaxLength(1000)
    @Column(length = 1000)
    public String bio;

    @Required
    public String twitterId;

    @Required
    public String pictureUrl;

}
