name := "playconf"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  "mysql" % "mysql-connector-java" % "5.1.26",
  "org.webjars" % "bootstrap" % "3.0.3"
)     

play.Project.playJavaSettings
